# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160423143354) do

  create_table "records", force: :cascade do |t|
    t.string   "site_serial"
    t.date     "date"
    t.float    "streamflow"
    t.float    "streamflow_spike"
    t.float    "water_temperature"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.float    "gdd"
    t.float    "predicted_length_c"
    t.float    "predicted_length_h"
    t.float    "predicted_length_m"
  end

  create_table "sites", force: :cascade do |t|
    t.string   "serial"
    t.string   "name"
    t.float    "reg_square"
    t.float    "reg_linear"
    t.float    "reg_const"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.float    "river_length"
  end

end
