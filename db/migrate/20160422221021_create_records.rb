class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.integer :site_id
      t.date :date
      t.float :streamflow
      t.float :streamflow_spike
      t.float :water_temperature
      t.timestamps null: false
    end
  end
end
