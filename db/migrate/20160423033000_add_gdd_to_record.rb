class AddGddToRecord < ActiveRecord::Migration
  def change
    add_column :records, :gdd, :float, null: true
  end
end
