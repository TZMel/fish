class AddPredictedLengthToRecord < ActiveRecord::Migration
  def change
    add_column :records, :predicted_length_c, :float, null: true
    add_column :records, :predicted_length_h, :float, null: true
    add_column :records, :predicted_length_m, :float, null: true
  end
end
