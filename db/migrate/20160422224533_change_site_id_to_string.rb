class ChangeSiteIdToString < ActiveRecord::Migration
  def change
    change_column :records, :site_id, :string
    rename_column :records, :site_id, :site_serial
  end
end
