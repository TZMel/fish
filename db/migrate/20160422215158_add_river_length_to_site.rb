class AddRiverLengthToSite < ActiveRecord::Migration
  def change
    add_column :sites, :river_length, :float
  end
end
