class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :serial
      t.string :name
      t.float :reg_square, null: true
      t.float :reg_linear, null: true
      t.float :reg_const, null: true
      t.timestamps null: false
    end
  end
end
