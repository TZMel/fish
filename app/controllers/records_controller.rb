class RecordsController < ApplicationController
     def index
        @records = Record.all
    end
    def new
        @record = Record.new
    end
    def create
        @record = Record.new(record_params)
        past_record = Record.where(:site_serial => @record.site_serial, :date => @record.date).first
        if (!past_record)
            @record.save
        end

        redirect_to :action => :index
    end
    def destroy
        @record = Record.find(params[:id])
        @record.destroy

        redirect_to :action => :index
    end

    private
    def record_params
        params.require(:record).permit(:site_serial, :date, :streamflow, :streamflow_spike, :water_temperature)
    end

end
