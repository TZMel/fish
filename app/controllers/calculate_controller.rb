class CalculateController < ApplicationController
    def day
        past_record = Record.where(:site_serial => params[:sites], :date => params[:date]).first
        if (! past_record)
            @record = Record.new
            require 'open-uri'
            url = 'http://waterservices.usgs.gov/nwis/iv/?format=json&sites=' + params[:sites] + '&startDT=' + params[:date] + '&endDT=' + params[:date] + '&parameterCd=00055&siteType=ST'
            response = open(url).read

            decode = ActiveSupport::JSON.decode(response)
            streamflows = decode['value']['timeSeries'][0]['values'][0]['value']

            count = 0
            sum = 0
            max = 0

            streamflows.each do |streamflow|
              sum = sum + streamflow['value'].to_f.round(1)
              count = count + 1
              if max < streamflow['value'].to_f.round(1)
                max = streamflow['value'].to_f.round(1)
              end
            end
            @record.site_serial = params[:sites]
            @record.date = params[:date]
            if (count != 0)
                @record.streamflow = (sum/count).round(1)
            else
                @record.streamflow = 0;
            end
            @record.streamflow_spike = max

            url_temp = 'http://waterservices.usgs.gov/nwis/iv/?format=json&sites=' + params[:sites] + '&startDT=' + params[:date] + '&endDT=' + params[:date] + '&parameterCd=00010&siteType=ST'
            response_temp = open(url_temp).read
            decode_temp = ActiveSupport::JSON.decode(response_temp)
            streamflows_temp = decode_temp['value']['timeSeries'][0]['values'][0]['value']
            sum_temp = 0
            count_temp = 0
            streamflows_temp.each do |streamflow_temp|
              sum_temp = sum_temp + streamflow_temp['value'].to_f.round(1)
              count_temp = count_temp + 1
            end

            @record.water_temperature = (sum_temp/count_temp).round(1)
            if ( @record.water_temperature > 15)
                water_temperature_greater_than_threshold = @record.water_temperature - 15
            else
                water_temperature_greater_than_threshold = 0
            end
            if (params[:date].to_date.strftime("%m%d") == '0101')
                @record.gdd = water_temperature_greater_than_threshold
            elsif (yesterday_record = Record.where(:site_serial => params[:sites], :date => params[:date].to_date.yesterday).first)
                @record.gdd = (water_temperature_greater_than_threshold + yesterday_record.gdd).round(2)
            end

            if (@record.water_temperature != 0)
                @record.predicted_length_c = (3.6*0.3048*@record.streamflow*3600*18779/@record.water_temperature/@record.water_temperature).round(1)
                @record.predicted_length_h = (3.6*0.3048*@record.streamflow*3600*21311/@record.water_temperature/@record.water_temperature/@record.water_temperature).round(1)
                @record.predicted_length_m = (3.6*0.3048*@record.streamflow*3600*233855/@record.water_temperature/@record.water_temperature/@record.water_temperature).round(1)
            else
                @record.predicted_length_c = 0
                @record.predicted_length_h = 0
                @record.predicted_length_m = 0
            end

            @record.save
        end

        render :text => 'success'
    end

    def year
        past_record = Record.where(:site_serial => params[:sites], :date => params[:date]).first
        if (! past_record)
            require 'open-uri'
            moving = (params[:date].to_date.strftime("%Y")+'0101').to_date
            while(moving <= params[:date].to_date)
                if (! Record.where(:site_serial => params[:sites], :date => moving).first)
                    @record = Record.new
                    require 'open-uri'
                    url = 'http://waterservices.usgs.gov/nwis/iv/?format=json&sites=' + params[:sites] + '&startDT=' + moving.to_s + '&endDT=' + moving.to_s + '&parameterCd=00055&siteType=ST'
                    response = open(url).read

                    decode = ActiveSupport::JSON.decode(response)
                    streamflows = decode['value']['timeSeries'][0]['values'][0]['value']

                    count = 0
                    sum = 0
                    max = 0

                    streamflows.each do |streamflow|
                      sum = sum + streamflow['value'].to_f.round(1)
                      count = count + 1
                      if max < streamflow['value'].to_f.round(1)
                        max = streamflow['value'].to_f.round(1)
                      end
                    end
                    @record.site_serial = params[:sites]
                    @record.date = moving
                    if (count != 0)
                        @record.streamflow = (sum/count).round(1)
                    else
                        @record.streamflow = 0;
                    end
                    @record.streamflow_spike = max

                    url_temp = 'http://waterservices.usgs.gov/nwis/iv/?format=json&sites=' + params[:sites] + '&startDT=' + moving.to_s + '&endDT=' + moving.to_s + '&parameterCd=00010&siteType=ST'
                    response_temp = open(url_temp).read
                    decode_temp = ActiveSupport::JSON.decode(response_temp)
                    streamflows_temp = decode_temp['value']['timeSeries'][0]['values'][0]['value']
                    sum_temp = 0
                    count_temp = 0
                    streamflows_temp.each do |streamflow_temp|
                      sum_temp = sum_temp + streamflow_temp['value'].to_f.round(1)
                      count_temp = count_temp + 1
                    end

                    if (count_temp != 0)
                        @record.water_temperature = (sum_temp/count_temp).round(1)
                    else
                        @record.water_temperature = 0;
                    end
                    if ( @record.water_temperature > 15)
                        water_temperature_greater_than_threshold = @record.water_temperature - 15
                    else
                        water_temperature_greater_than_threshold = 0
                    end

                    if (moving.strftime("%m%d") == '0101')
                        @record.gdd = water_temperature_greater_than_threshold
                    elsif (yesterday_record = Record.where(:site_serial => params[:sites], :date => moving.yesterday).first)
                        @record.gdd = (water_temperature_greater_than_threshold + yesterday_record.gdd).round(2)
                    end

                    if (@record.water_temperature != 0)
                        @record.predicted_length_c = (3.6*0.3048*@record.streamflow*3600*18779/@record.water_temperature/@record.water_temperature).round(1)
                        @record.predicted_length_h = (3.6*0.3048*@record.streamflow*3600*21311/@record.water_temperature/@record.water_temperature/@record.water_temperature).round(1)
                        @record.predicted_length_m = (3.6*0.3048*@record.streamflow*3600*233855/@record.water_temperature/@record.water_temperature/@record.water_temperature).round(1)
                    else
                        @record.predicted_length_c = 0
                        @record.predicted_length_h = 0
                        @record.predicted_length_m = 0
                    end

                    @record.save
                end
                moving = moving.tomorrow
            end
            render :json => Record.where(:site_serial => params[:sites], :date => params[:date]).first
        else
            render :json => past_record
        end
    end
end
