class SitesController < ApplicationController
    def index
        @sites = Site.all
    end
    def new
        @site = Site.new
    end
    def create
        @site = Site.new(site_params)
        past_site = Site.where(:serial => @site.serial).first
        if (!past_site)
            @site.save
        end

        redirect_to :action => :index
    end
    def destroy
        @site = Site.find(params[:id])
        @site.destroy

        redirect_to :action => :index
    end

    def riverLength
        @site = Site.where('serial' => params[:serial]).first
        render :text => @site.river_length
    end

    private
    def site_params
        params.require(:site).permit(:serial, :name, :river_length, :reg_square, :reg_linear, :reg_const)
    end
end
