$(function() {
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 8, // Creates a dropdown of 15 years to control year
    min: new Date(2008, 01, 01),
    max: true,
    format: 'yyyy-mm-dd',
  });
});