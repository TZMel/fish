$(document).ready(function(){

  takit_conversations_list = new Vue({
    el: '#pika-search-result',

    data: {
      site: '04092750',
      date: '2016-01-01',
      streamflow: 0.0,
      streamflow_spike: 0.0,
      water_temperature: 0.0,
      gdd: 0.0,
      predicted_length_c: 0.0,
      predicted_length_h: 0.0,
      predicted_length_m: 0.0,
      river_length: 0.0,
      status: 'success',
    },

    computed: {
      url: function() {
        return 'specific?sites=' + this.site + '&date=' + this.date;
      },
      status_fish: function() {
        if (this.gdd > 650 && this.water_temperature >= 17)
          return 1
        else
          return 0;
      },
      status_c: function() {
        if (this.status_fish == 0 || this.river_length <= this.predicted_length_c)
          return 0;
        else if (this.gdd <= 900 && this.streamflow_spike*0.3048 <= 0.7)
          return 1;
        else if (this.gdd <= 900 && this.streamflow_spike*0.3048 > 0.7)
          return 2;
        else if (this.gdd > 900 && this.streamflow_spike*0.3048 <= 0.7)
          return 3;
        else
          return 4;
      },
      status_h: function() {
        if (this.status_fish == 0 || this.river_length <= this.predicted_length_h)
          return 0;
        else if (this.gdd <= 900 && this.streamflow_spike*0.3048 <= 0.7)
          return 1;
        else if (this.gdd <= 900 && this.streamflow_spike*0.3048 > 0.7)
          return 2;
        else if (this.gdd > 900 && this.streamflow_spike*0.3048 <= 0.7)
          return 3;
        else
          return 4;
      },
      status_m: function() {
        if (this.status_fish == 0 || this.river_length <= this.predicted_length_m)
          return 0;
        else if (this.gdd <= 900 && this.streamflow_spike*0.3048 <= 0.7)
          return 1;
        else if (this.gdd <= 900 && this.streamflow_spike*0.3048 > 0.7)
          return 2;
        else if (this.gdd > 900 && this.streamflow_spike*0.3048 <= 0.7)
          return 3;
        else
          return 4;
      },
    },

    methods: {
      loadData: function() {
        this.status = 'loading';
        this.$http.get(this.url, function(data, status, request) {
          if (status == 200) {
           this.streamflow = data.streamflow;
           this.streamflow_spike = data.streamflow_spike;
           this.water_temperature = data.water_temperature;
           this.gdd = data.gdd;
           this.predicted_length_c = data.predicted_length_c;
           this.predicted_length_h = data.predicted_length_h;
           this.predicted_length_m = data.predicted_length_m;
           this.status = 'success';
          }
        }).error(() => { this.status = 'fail' });

        this.$http.get('river-length?serial='+ this.site, function(data, status, request) {
          if (status == 200) {
            this.river_length = data;
          }
        }).error(() => {});
      },
    },

    ready: function() {
      this.loadData();
    },

  });
});




